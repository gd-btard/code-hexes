"use strict";

H.draw = (function() {
    const terrainColors = {
        land: 0x304f00
    };
    
    const runeTint = function(type) {        
        //if(type == 'fire') return 0xffa387;
        //if(type.includes('avatar')) return 0x87a1ff;
        return 0xffffff;
    };

    return {
        load_assets(cb) {
            const loader = new PIXI.loaders.Loader();

            loader.add('rune/nop', 'img/runes/nop.png')
                  .add('rune/fire', 'img/runes/fire.png')
                  .add('rune/arrow0', 'img/runes/arrow0.png')
                  .add('rune/arrow60', 'img/runes/arrow60.png')
                  .add('rune/arrow120', 'img/runes/arrow120.png')
                  .add('rune/move', 'img/runes/move.png')
                  .add('rune/moveDir', 'img/runes/moveDir.png')                  
                  .add('rune/left', 'img/runes/left.png')
                  .add('rune/right', 'img/runes/right.png')
                  .add('rune/if', 'img/runes/if.png')
                  .add('rune/bit', 'img/runes/bit.png')
                  .add('rune/bitPlus', 'img/runes/bitPlus.png')
                  .add('rune/bitQuestion', 'img/runes/bitQuestion.png')
                  .add('rune/1', 'img/runes/1.png')
                  .add('rune/2', 'img/runes/2.png')
                  .add('rune/3', 'img/runes/3.png')
                  .add('rune/4', 'img/runes/4.png')
                  .add('rune/5', 'img/runes/5.png')
                  .add('rune/6', 'img/runes/6.png')                  
                  .add('obj/mageStatue', 'img/objs/mageStatue.png')
                  .add('obj/bit', 'img/objs/bit.png')
                  .add('shader/glowRune', 'shaders/glowRune.frag');           

            loader.load((loader, resources) => {               
                this.assets = resources;
                this.filters = {}
                this.filters.glowRune = new PIXI.Filter(null, resources['shader/glowRune'].data);
                this.filters.glowRune.uniforms.strength = 0.8;     
                cb();
            });
        },

        rune(container, ij, rune) {
            let name, rot, flip;

            switch(rune.type) {
                case 'arrow': 
                    let diff = Math.abs(rune.to - rune.from); 
                    if(diff > 3) diff = 6 - diff; //3 - 0 deg, 2 - 60 deg, 1 - 120 deg
                    const isLeft = (rune.from + diff) % 6 == rune.to;
                    
                    switch(diff) {
                        case 3: name = 'arrow0'; break;
                        case 2: name = 'arrow60'; break;
                        case 1: name = 'arrow120'; break;
                    }

                    rot = (rune.from * 60) / 180 * Math.PI;
                    flip = isLeft;                
                    break;

                case 'move':
                case 'if':
                    name = rune.type;
                    rot =   (rune.from * 60) / 180 * Math.PI;
                    flip = false;
                    break;
                default: 
                    name = rune.type;
                    rot = 0;
                    flip = false;
            }           
            
            const spr = new PIXI.TilingSprite(this.assets['rune/' + name].texture, 128, 128);
            spr.pivot.x = spr.pivot.y = 64;        
            spr.scale.x = spr.scale.y = H.settings.cellSize * 0.9 / 128;     
            spr.rotation = rot;
            spr.tint = runeTint(rune.type);
            if(flip)
                spr.scale.x = -spr.scale.x;

            const [i,j] = ij;
            const coords = H.vector.add(H.vector.mul(i, H.hex_math.basis[0]), H.vector.mul(j, H.hex_math.basis[1]));            
            spr.x = coords[0] * H.settings.cellSize;
            spr.y = coords[1] * H.settings.cellSize;

            if('glow' in rune)
                spr.filters = [this.filters.glowRune];
            container.addChild(spr);

            return spr;
        },

        object(container, ij, obj) {  
            const tex = this.assets['obj/' + obj.type].texture;
            const frames_w = Math.round(tex.width / 128);
            const frames_h = Math.round(tex.height / 128);            

            let spr;
            if(frames_w > 1 || frames_h > 1) {
                let textures = [];
                for(let i = 0; i < frames_w; i++)
                    for(let j = 0; j < frames_h; j++) {
                        let frame_tex = new PIXI.Texture(tex);
                        frame_tex.frame = new PIXI.Rectangle(128 * i, 128 * j, 128, 128);
                        frame_tex._updateUvs();
                        textures.push(frame_tex);
                    }
               
                spr = new PIXI.extras.AnimatedSprite(textures);
                spr.play();
                spr.animationSpeed = 0.1;
            }
            else {
                spr = new PIXI.TilingSprite(tex, 128, 128);
            }
            spr.pivot.x = spr.pivot.y = 64;    
            spr.scale.x = spr.scale.y = H.settings.cellSize * 0.9 / 128;     
            spr.rotation = (obj.rot * 60) / 180 * Math.PI;

            const [i,j] = ij;
            const coords = H.vector.add(H.vector.mul(i, H.hex_math.basis[0]), H.vector.mul(j, H.hex_math.basis[1]));            
            spr.x = coords[0] * H.settings.cellSize;
            spr.y = coords[1] * H.settings.cellSize;

            container.addChild(spr);
            return spr;
        },

        /** Draw all avatars for one cell */
        //TODO number if many of one type? what to do with several types?
        avatars(container, ij, avs) {
            const [i, j] = ij;
            const [ex, ey] = H.hex_math.basis;
            const [x, y] = H.vector.add(H.vector.mul(i, ex), H.vector.mul(j, ey));  

            let size = 0.8;
            let step = 0.1;

            const lineCont = new PIXI.Container();

            for(const a of avs) {
                const color = a.if ? 0xFF00FF : 0xFF0000;

                this.hex_contour(lineCont, [1, color, 1], ij, size);
                
                let [dx, dy] = H.hex_math.neighbours[a.dir];
                [dx, dy] = H.vector.add(H.vector.mul(dx, ex), H.vector.mul(dy, ey));

                const dirLine = new PIXI.Graphics();
                dirLine.lineStyle(1, 0xFF0000, 1);
                dirLine.moveTo(x * H.settings.cellSize, y * H.settings.cellSize);
                dirLine.lineTo((x + 0.2 * dx) * H.settings.cellSize, (y + 0.2 * dy) * H.settings.cellSize);
                lineCont.addChild(dirLine);

                size -= step;
                if(size < 0) 
                    break;
            }

            container.addChild(lineCont);
            return lineCont;
        },

        cell(container, ij, cell, offset = 0.95) {
            let [i, j] = ij;
            let [ex, ey] = H.hex_math.basis;
            let [x, y] = H.vector.add(H.vector.mul(i, ex), H.vector.mul(j, ey));

            const poly = new PIXI.Graphics();
            const color = terrainColors[cell.type];
            poly.beginFill(color, 1); 

            let coords = []
            for(let i = 0; i < 6; i++) {
                let [vx, vy] = H.hex_math.vertices[i];
                coords.push((x + vx * offset) * H.settings.cellSize);
                coords.push((y + vy * offset) * H.settings.cellSize);                
            }

            poly.drawPolygon(coords);           
            poly.endFill();            

            container.addChild(poly);            

            return poly;
        }, 

        rulers(cont) {      
            const alpha = 0.1;
            
            let zx = new PIXI.Graphics();
            zx.lineStyle(1, 0xFFFFFF, alpha);
            zx.moveTo(-10000 * H.hex_math.basis[0][0], -10000 * H.hex_math.basis[0][1]);
            zx.lineTo(10000 * H.hex_math.basis[0][0], 10000 * H.hex_math.basis[0][1]);    
            cont.addChild(zx);

            let zy = new PIXI.Graphics();
            zy.lineStyle(1, 0xFFFFFF, alpha);
            zy.moveTo(-10000 * H.hex_math.basis[1][0], -10000 * H.hex_math.basis[1][1]);
            zy.lineTo(10000 * H.hex_math.basis[1][0], 10000 * H.hex_math.basis[1][1]);    
            cont.addChild(zy);

            let zv = new PIXI.Graphics();
            zv.lineStyle(1, 0xFFFFFF, alpha);
            zv.moveTo(0, -10000);
            zv.lineTo(0, 10000);    
            cont.addChild(zv);
        },

        hex_contour(container, lineStyle, ij, size) {
            const [i, j] = ij;
            const [ex, ey] = H.hex_math.basis;
            const [x, y] = H.vector.add(H.vector.mul(i, ex), H.vector.mul(j, ey));  
                        
            for(let i = 0; i < 6; i++) {
                const [dx0, dy0] = H.hex_math.vertices[i];
                const [dx1, dy1] = H.hex_math.vertices[(i + 1) % 6]; 

                let line = new PIXI.Graphics();
                line.lineStyle(...lineStyle);
                line.moveTo((x + dx0 * size) * H.settings.cellSize, (y + dy0 * size) * H.settings.cellSize);
                line.lineTo((x + dx1 * size) * H.settings.cellSize, (y + dy1 * size) * H.settings.cellSize);
                container.addChild(line);
            }
        },
    }    
})(); 