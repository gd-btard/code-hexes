"use strict";

H.vector = (function() {
    return {
        add(a, b) {
            return [a[0] + b[0], a[1] + b[1]];
        },

        sub(a, b) {
            return [a[0] - b[0], a[1] - b[1]];
        },
        
        mul(k, a) {
            return [k * a[0], k * a[1]];
        },

        div(a, k) {            
            return this.mul(1 / k, a);
        },

        length(a) {
            return Math.sqrt(a[0] * a[0] + a[1] * a[1]);
        }
    }    
})();