"use strict";

H.input = function() {
    const areaWidth = H.game.pixi.renderer.width;
    const areaHeight = H.game.pixi.renderer.height;

    H.game.pixi.stage.interactive = true;
    H.game.pixi.stage.hitArea = new PIXI.Rectangle(0, 0, areaWidth, areaHeight);

    let prevPos = null;    

    const listeners = {
        rightdrag: [],
        leftclick: [],
        move: [],
        keydown: []
    };

    const coordsInArea = function(coords) {
        return coords[0] >= 0 && coords[0] < areaWidth &&
               coords[1] >= 0 && coords[1] < areaHeight;
    }

    const raise = function(event, ...args) {
        for(let l of listeners[event])
            l(...args);
    }

    const mousedown = function(button, coords) {
        prevPos = coords;

        if(!coordsInArea(coords))
            return;

        if(button == 'left') 
            raise('leftclick', coords);
    };

    const mouseup = function(button) {
        
    };

    const mousemove = function(button_flags, coords) {          
        const right = (button_flags & 2) == 2;
        const left = (button_flags & 1) == 1;        
        
        //Right drag works outside area
        //TODO: add "mouse capture" (drag only if we started inside area)
        if(right) 
            raise('rightdrag', H.vector.sub(coords, prevPos));         
        
        if(coordsInArea(coords)) {        
            raise('move', coords, left, right);
        }
        
        prevPos = coords;
    };

    H.game.pixi.stage.on('mousemove', function(event) { mousemove(event.data.buttons, [event.data.global.x, event.data.global.y]);})
    H.game.pixi.stage.on('mousedown', function(event) { mousedown('left', [event.data.global.x, event.data.global.y]); } );
    H.game.pixi.stage.on('mouseup', function(event)   { mouseup('left'); } );
    H.game.pixi.stage.on('rightdown', function(event) { mousedown('right', [event.data.global.x, event.data.global.y]); } ); 
    H.game.pixi.stage.on('rightup', function(event)   { mouseup('right'); } ); 

    const inp = {
        on(eventName, handler) {
            listeners[eventName].push(handler);
        },

        shift: false,
        ctrl: false
    };

    window.addEventListener('keydown', function(x) 
    { 
        if(x.key == "Shift") inp.shift = true;
        if(x.key == "Control") inp.ctrl = true;
        raise('keydown', x.key, x.keyCode);         
    }, false);

    window.addEventListener('keyup', function(x) 
    { 
        if(x.key == "Shift") inp.shift = false;
        if(x.key == "Control") inp.ctrl = false;     
    }, false);

    return inp;
};