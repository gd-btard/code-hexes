"use strict";

H.map = (function () {
    const drawGraphics = function(ij, cell) {
        H.game.map.graphics[ij] = {
            back: H.draw.cell(H.game.pixi.gridCont, ij, cell),
            objs: []          
        };

        if(cell.objects) {
            for(const o of cell.objects) 
                H.game.map.graphics[ij].objs.push(H.draw.object(H.game.pixi.gridCont, ij, o));
        }

        if(cell.rune) {
            H.game.map.graphics[ij].objs.push(H.draw.rune(H.game.pixi.gridCont, ij, cell.rune));
        }

        if(cell.avatars) {            
            H.game.map.graphics[ij].objs.push(H.draw.avatars(H.game.pixi.gridCont, ij, cell.avatars));
        }
    };

    const eraseGraphics = function(ij) {
        H.game.pixi.gridCont.removeChild(H.game.map.graphics[ij].back);
        for(const o of H.game.map.graphics[ij].objs) 
            H.game.pixi.gridCont.removeChild(o);

        delete  H.game.map.graphics[ij];
    };

    return {
        create() {
            H.game.map = {
                cells: {},
                graphics: {}
            }
        },
        
        getCell(i, j) {
            if(!([i,j] in H.game.map.cells))
                return { type: 'none' };
            
            return H.game.map.cells[[i,j]];
        },

        /** This will redraw cell completely for simplicity */
        setCell(i, j, cell) {
            const key = [i,j];
            if(key in H.game.map.cells) 
                eraseGraphics(key);
            
            if(cell.type == 'none')  {
                delete H.game.map.cells[key];
            }
            else {
                H.game.map.cells[key] = cell;
                drawGraphics(key, cell);
            }
        },

        /** This ensures proper redraw, cell should be changed in changeFunc or returned new cell */
        changeCell(i, j, changeFunc) {
            let cell = this.getCell(i, j);
            cell = changeFunc(cell) || cell;
            this.setCell(i, j, cell);
        },        

        saveToLocalstorage(name) {
            localStorage.setItem('map-' + name, JSON.stringify({ cells: H.game.map.cells }));
        },

        loadFromLocalstorage(name) {  
            H.game.pixi.gridCont.children.length = 0;
            const mapStr = localStorage.getItem('map-' + name);
            if(mapStr == null) {
                console.log("No map with a name " + name + " found");
                this.create();
                return;
            }

            H.game.map = {
                cells: JSON.parse(mapStr).cells,
                graphics: {}
            };

            for (let [key,cell] of _.entries(H.game.map.cells)) {
                let cell = H.game.map.cells[key];
                key = H.util.pos_str_to_arr(key);
                drawGraphics(key, cell);
            }
        },

        deleteFromLocalstorage(name) {
            localStorage.removeItem('map-' + name);
        }
    }
})();