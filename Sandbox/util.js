"use strict";

H.util = {
    pos_str_to_arr(p) {
        return _.map(p.split(','), x => parseInt(x));
    },

    first_obj_of_type(cell, type) {
        return _.head(_.filter(cell.objects, y => y.type == type));
    },

    //There functions delete property in case of zero array length
    push_to_prop_arr(obj, prop, val) {
        if(!(prop in obj))
            obj[prop] = [];
        obj[prop].push(val);
    },

    pull_from_prop_arr(obj, prop, val) {
        _.pull(obj[prop], val);
        if(obj[prop].length == 0)
            delete obj[prop];
    }  
};