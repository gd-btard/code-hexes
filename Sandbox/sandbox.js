"use strict";

(function() { 
    const screenDiv = $("#pixi-screen")[0];
    const screenRect = screenDiv.getBoundingClientRect();    

    H.game = { pixi: {} }

    H.game.pixi.renderer = PIXI.autoDetectRenderer(screenRect.width, screenRect.height, { antialias: true, transparent: true, resolution: 1 });            
    H.game.pixi.renderer.view.style.margin = "0";
    screenDiv.appendChild(H.game.pixi.renderer.view);
    H.game.pixi.stage = new PIXI.Container();   

    H.game.pixi.surfaceCont = new PIXI.Container();
    H.game.pixi.stage.addChild(H.game.pixi.surfaceCont);   
   
    //Map
    H.game.pixi.gridCont = new PIXI.Container();
    H.game.pixi.surfaceCont.addChild(H.game.pixi.gridCont);

    H.draw.rulers(H.game.pixi.surfaceCont);

    //Center it to viewport
    H.game.pixi.surfaceCont.x = screenRect.width / 2;
    H.game.pixi.surfaceCont.y = screenRect.height / 2;  
    

    H.settings = {
        cellSize: 60
    };

    H.input = H.input();
    H.level_editor = H.level_editor();

    //Panning
    H.input.on('rightdrag', function(offset) {
        H.game.pixi.surfaceCont.x += offset[0];
        H.game.pixi.surfaceCont.y += offset[1];        
    });  

    //We need to wait for assets
    H.draw.load_assets(() => {
        H.map.loadFromLocalstorage('1');
        if(H.game.map == null) {
            H.map.create();
        }

        //debug        
        //H.map.changeCell(2, 2, x => { x.avatars = [{}, {}, {}] });

        H.spells.init();
    
        const gameLoop = function () {
            H.game.pixi.renderer.render(H.game.pixi.stage); 
            requestAnimationFrame(gameLoop);
        };
    
        gameLoop();
    });
    
})();



