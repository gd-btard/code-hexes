"use strict";

H.level_editor = function() {
    const brushToCell = function(brush) {
        switch(brush) {
            case 'land': return { type: 'land' }
            case 'erase': return { type: 'none' }
        }
    };

    const coordsToIJ = function(coords) {
        let c = H.game.pixi.gridCont.toLocal(new PIXI.Point(...coords), H.stage);
        c = [c.x, c.y];
        c = H.vector.div(c, H.settings.cellSize);        
        return H.hex_math.planeCoordsToHexCell(c);
    };

    const viewModel = {
        instrument: 'terrain',
        mapName: '',        

        doMapNew() {
            H.game.pixi.gridCont.children.length = 0;
            H.map.create();
        },

        doMapSave() {
            for(const s of H.game.spells.spells)
                H.spells.destroySpell(s);

            H.map.saveToLocalstorage(this.mapName);
        },

        doMapLoad() {
            H.map.loadFromLocalstorage(this.mapName);            
        },

        doMapDelete() {
            H.map.deleteFromLocalstorage(this.mapName);
        },

        brush: 'land',
        brushSize: 1,

        coerceBrushSize() {
            if(this.brushSize.length == 0) 
                return;

            this.brushSize = Math.max(1, Math.min(9, parseInt(this.brushSize)));
            if(isNaN(this.brushSize)) 
                this.brushSize = 1;
        },

        paint(ij) {
            let [i,j] = ij;
            let sz = this.brushSize || 1;            
            
            let cell = brushToCell(this.brush);
            for(let dx = -sz; dx <= sz; dx++)
                for(let dy = -sz; dy <= sz; dy++) {                    
                    if(H.hex_math.cellDist(dx, dy) < sz) {
                        let exCell = H.map.getCell(i + dx, j + dy);
                        
                        //Just change the type, leave other things in place
                        if(cell.type != exCell.type) {
                            exCell.type = cell.type;
                            H.map.setCell(i + dx, j + dy, exCell);
                        }
                    }                    
                }
        },

        setObject(ij) {
            let [i,j] = ij;
            let cell = H.map.getCell(i, j);
            
            let exObj = 'objects' in cell ? _.head(_.filter(cell.objects, x => x.type == this.object)) : null;            

            //Rotate
            if(exObj && H.input.shift) {
                exObj.rot = (exObj.rot + 1) % 6;
                H.map.setCell(i, j, cell);
            }
            //Delete
            else if(exObj && H.input.ctrl) {
                H.util.pull_from_prop_arr(cell, 'objects', exObj);                
            }
            //Add
            else if(!exObj) {
                const o = {
                    type: this.object,
                    rot: 0
                };
                H.util.push_to_prop_arr(cell, 'objects', o);                
            }

            H.map.setCell(i, j, cell);  
        },

        setRune(ij) {
            let [i,j] = ij;
            let cell = H.map.getCell(i, j);
            let rune = cell.rune;           

            //Rotate or flip runes
            if((H.input.shift || H.input.ctrl) && rune != null && H.spells.rotatableRunes.includes(rune.type)) {
                if(H.input.shift) { //Rotate
                    rune.from = (rune.from + 1) % 6;
                    rune.to = (rune.to + 1) % 6;                 
                }
                else { //Flip
                    let diff = Math.abs(rune.to - rune.from); 
                    if(diff > 3) diff = 6 - diff; //3 - 0 deg, 2 - 60 deg, 1 - 120 deg
                    const isLeft = (rune.from + diff) % 6 == rune.to;
                    if(isLeft) rune.to = (rune.from - diff + 6) % 6;
                    else rune.to = (rune.from + diff) % 6;
                }

                H.map.setCell(i, j, cell);
            }
            else {
                //Erase rune
                if(viewModel.rune == 'none') {
                    delete cell.rune;
                    H.map.setCell(i, j, cell);
                }
                //Draw new or replace rune
                else {
                    let newRune = {};
                    switch(viewModel.rune) {
                        case 'arrow0':                        
                            newRune = { type: 'arrow' };    
                            newRune.from = 0;
                            newRune.to = 3;
                            break;
                        case 'arrow60':
                            newRune = { type: 'arrow' };    
                            newRune.from = 0;
                            newRune.to = 4;
                            break;
                        case 'arrow120':                        
                            newRune = { type: 'arrow' };    
                            newRune.from = 0;
                            newRune.to = 5;
                            break;
                        case 'move':
                        case 'if':
                            newRune = { type: viewModel.rune }
                            newRune.from = 0;
                            newRune.to = 3;
                            break;
                        default: 
                            newRune = { type: viewModel.rune }
                            break;
                    }
                    
                    if(rune == null || rune.type != newRune.type) {
                        cell.rune = newRune;              
                        H.map.setCell(i, j, cell);
                    }
                }
            }             
        },

        runes: ['nop', 'fire', 'arrow0', 'arrow60', 'arrow120', 'move', 'moveDir',
                'left', 'right', 'if', 'bit', 'bitPlus', 'bitQuestion', '1', '2', '3', '4', '5', '6'],
        rune: 'arrow0',

        objects: ['mageStatue', 'bit'],
        objects_frames: [[1,1],[2,2]],
        
        object: 'mageStatue',
        

        spell: null,
        testSpellStep() {            
            if(viewModel.spell == null) {
                const [pos, statue] = 
                    _(_.entries(H.game.map.cells))
                    .map(([pos, cell]) => [H.util.pos_str_to_arr(pos), 
                                           H.util.first_obj_of_type(cell, 'mageStatue')]) 
                    .filter(([pos, statue]) => statue)
                    .head();                
                
                const castDir = H.hex_math.opposite_dir(statue.rot);
                const castPos = H.hex_math.neigh_to_dir(pos, castDir);                
                viewModel.spell = H.spells.launch([0,0], castPos, castDir);
            }
            else
                H.spells.step(viewModel.spell);
        },

        testSpellReset() {
            if(viewModel.spell) {
                H.spells.destroySpell(viewModel.spell);
                viewModel.spell = null;
            }
        },

        //debug
        input: H.input,
        cursorX: 0,
        cursorY: 0
    };

    H.input.on('keydown', function(key) {
        if(key == '1' || key == '2') {
            if(viewModel.brushSize.length == 0) return;
            if(document.activeElement.tagName == 'INPUT') return;
        }

        switch(key) {
            case '1':                
                viewModel.brushSize = Math.max(1, viewModel.brushSize - 1);
                break;
            case '2':                
                viewModel.brushSize = Math.min(9, viewModel.brushSize + 1);
                break;   
            
            case 'c':
                viewModel.brush = 'land';
                break;
            
            case 'v':
                viewModel.brush = 'erase';
                break;
        }
    });

    H.input.on('leftclick', function(coords) {
        $("#pixi-screen")[0].focus();

        switch(viewModel.instrument) {
            case 'terrain': viewModel.paint(coordsToIJ(coords)); break;
            case 'runes': viewModel.setRune(coordsToIJ(coords)); break;
            case 'objs': viewModel.setObject(coordsToIJ(coords)); break;
        }     
    });       
    
    H.input.on('move', function(coords, left, right) {
        const [x, y] = coordsToIJ(coords);

        if(left && viewModel.instrument == 'terrain') {            
            viewModel.paint([x, y]);
        }       

        viewModel.cursorX = x;
        viewModel.cursorY = y;
    });

    new Vue({
         el: "#level-editor",
         data: viewModel
    });

    return viewModel;
};