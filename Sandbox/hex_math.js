"use strict";

H.hex_math = (function() {
    const r3 = Math.sqrt(3);
    const ex = [r3 * 0.5, 0.5]; //basis
    const ey = [-r3 * 0.5, 0.5];
    const iex = [1 / r3, -1 / r3]; //inverse basis
    const iey = [1, 1];

    return {
        basis: [ex, ey],   
        neighbours: [[1,1], [0,1], [-1,0], [-1,-1], [0,-1], [1,0]],

        vertices: [
            [-0.5 / r3, 0.5],
            [0.5 / r3, 0.5],
            [1. / r3, 0],
            [0.5 / r3, -0.5],
            [-0.5 / r3, -0.5],
            [-1 / r3, 0]
        ],
        
        planeCoordsToHexCell(c) {
            //this just searches for the nearest center of 4 possible hex cells 
            //inside a basis (ex, ey) "square" (a rhombus) of [i,j,i+1,j+1]
            let [cx, cy] = c;
            let x = cx * iex[0] + cy * iey[0];
            let y = cx * iex[1] + cy * iey[1];
            let ix = Math.floor(x);
            let iy = Math.floor(y);            
            let de = [x - ix, y - iy];
            let d = H.vector.add(H.vector.mul(de[0], ex), H.vector.mul(de[1], ey));
            let d00 = H.vector.length(d);
            let d10 = H.vector.length(H.vector.sub(ex, d));
            let d01 = H.vector.length(H.vector.sub(ey, d));
            let d11 = H.vector.length(H.vector.sub(H.vector.add(ex, ey), d));
            let mind = d00; let mini = 0;

            if (d10 < mind) { mind = d10; mini = 1; }
            if (d01 < mind) { mind = d01; mini = 2; }
            if (d11 < mind) { mind = d11; mini = 3; }
            switch (mini)
            {
                case 0: return [ix, iy];
                case 1: return [ix + 1, iy];
                case 2: return [ix, iy + 1];
                case 3: return [ix + 1, iy + 1];            
            }
        },

        cellDist(dx, dy) {
            if ((dx < 0) == (dy < 0))
                return Math.max(Math.abs(dx), Math.abs(dy));
            else
                return Math.abs(dx - dy);
        }, 

        are_dirs_opposite(d1, d2) {
            return ((d1 + 3) % 6) == d2;
        },

        opposite_dir(dir) {
            return (dir + 3) % 6;
        },
    
        neigh_to_dir(pos, dir) {
            return H.vector.add(pos, H.hex_math.neighbours[dir]);
        }
    }    
})(); 