H.spells = (function() {
    const init = function() {
        H.game.spells = 
        {
            glowRunes: [],
            spells: [],
            lastSpellId: 0
        };
    };

    const getGlowRunes = function(spells) {
        return _(spells).map(x => x.avatars).flatten()
                        .map(x => x.runePos).uniqWith(_.isEqual).value();
    };

    const updateGlowRunes = function() {
        //Just save old glowRunes position and off-update-on everything        
        for(const [i,j] of H.game.spells.glowRunes)
            H.map.changeCell(i, j, c => { delete c.rune.glow; });        

        H.game.spells.glowRunes = getGlowRunes(H.game.spells.spells);        

        for(const [i,j] of H.game.spells.glowRunes) 
            H.map.changeCell(i, j, c => { c.rune.glow = {}; });        
    }

    

    /** Returns { pos:[i,j], dir, rune } */
    const getNeighRunes = function(pos) {
        const runes = [];

        for(const [d, [dx,dy]] of _.map(H.hex_math.neighbours, (x,i) => [i,x])) {
            const [i,j] = [pos[0] + dx,  pos[1] + dy];
            const cell = H.map.getCell(i, j);
            if('rune' in cell) 
                runes.push({ pos: [i,j], dir: d, rune: cell.rune });  
        }

        return runes;
    };

    const createAvatar = function(spell, avatar) {
        avatar.spellId = spell.id;        
        spell.lastAvatarId++;
        avatar.id = spell.lastAvatarId;
        spell.avatars.push(avatar);
        H.map.changeCell(avatar.pos[0], avatar.pos[1], cell => { H.util.push_to_prop_arr(cell, 'avatars', avatar); });           
    };

    const moveAvatar = function(spell, avatar, newPos) {
        const destCell = H.map.getCell(newPos[0], newPos[1]);
        if(destCell.type == 'none') {
            dieAvatar(spell, avatar);
        }
        else {
            H.map.changeCell(avatar.pos[0], avatar.pos[1], cell => { H.util.pull_from_prop_arr(cell, 'avatars', avatar); });
            avatar.pos = newPos;
            H.map.changeCell(avatar.pos[0], avatar.pos[1], cell => { H.util.push_to_prop_arr(cell, 'avatars', avatar); });
        }
    };

    const forkAvatar = function(spell, avatar) {        
        const a = _.cloneDeep(avatar);
        createAvatar(spell, a);
        return a;
    };

    const dieAvatar = function(spell, avatar) {        
        H.map.changeCell(avatar.pos[0], avatar.pos[1], cell => { H.util.pull_from_prop_arr(cell, 'avatars', avatar); });
        _.pull(spell.avatars, avatar);
        updateGlowRunes();
        //console.log(`Av ${avatar.id} died`);        
    };

    const chooseNextInstr = function(spell, avatar) {
        const rune = H.map.getCell(avatar.runePos[0], avatar.runePos[1]).rune;
       
        let nextRunes;        
        if(rune.type == 'arrow') { 
            //Go to arrow dir
            const nextPos = H.hex_math.neigh_to_dir(avatar.runePos, rune.to);
            const nextCell = H.map.getCell(nextPos[0], nextPos[1]);
            const nextRune = nextCell.rune;            
            nextRunes = nextRune ? [{ pos: nextPos, dir: rune.to, rune: nextRune }] : [];
        }        
        else {        
            //Find outgoing arrows    
            const runes = getNeighRunes(avatar.runePos);
            const fromArrows = _.filter(runes, x => x.rune.type == 'arrow' && H.hex_math.are_dirs_opposite(x.dir, x.rune.from));              
            
            if(fromArrows.length > 0)
                nextRunes = fromArrows;
            else {
                //Find one and only one neighbour
                nextRunes = _.filter(runes, x => x.rune.type != 'arrow' && !_.isEqual(x.pos, avatar.prevRunePos));
                if(nextRunes.length > 1) {
                    spell.error = { msg: 'Multiple next runes', pos: avatar.runePos };
                    console.log(spell.error.msg);
                    return false;
                }
            }
        }

        if(nextRunes.length == 0) {
            dieAvatar(spell, avatar);
            updateGlowRunes();
            return false;
        }

        for(const [i, r] of nextRunes.entries()) {
            //Move forked avatars first, original last to avoid errors            
            let av = (i == nextRunes.length - 1) ? avatar : forkAvatar(spell, avatar); 
            av.prevRunePos = av.runePos;
            av.runePos = r.pos;        

            //console.log(`Av ${av.id} step from ${av.prevRunePos} to ${av.runePos}`);
        }

        return true;
    };

    const execInstr = function(spell, avatar) {
        const rune = H.map.getCell(avatar.runePos[0], avatar.runePos[1]).rune;

        if(rune.type == 'arrow' || rune.type == 'nop') {
            //Do nothing
        } else {
            const avCell = H.map.getCell(avatar.pos[0], avatar.pos[1]);            
            switch(rune.type) {
                case 'right': 
                    avatar.dir = (avatar.dir + 1) % 6;
                    H.map.setCell(avatar.pos[0], avatar.pos[1], avCell);
                    break;
                case 'left': 
                    avatar.dir--;
                    if(avatar.dir < 0) avatar.dir = 5;
                    H.map.setCell(avatar.pos[0], avatar.pos[1], avCell);
                    break;
                case 'moveDir':
                    moveAvatar(spell, avatar, H.hex_math.neigh_to_dir(avatar.pos, avatar.dir));
                    break;   
                case 'move':
                    moveAvatar(spell, avatar, H.hex_math.neigh_to_dir(avatar.pos, H.hex_math.opposite_dir(rune.from)));
                    break;
            }
        }   
        
        updateGlowRunes();
        return null;
    };

    const stepAvatar = function(spell, avatar) {    
        const success = chooseNextInstr(spell, avatar);
        if(!success) return;
        const stackAv = execInstr(spell, avatar);
    };

    const launch = function(startRunePos, castPos, castDir) {
        const spellId = H.game.spells.lastSpellId;
        const spell = {  
            id: spellId,
            lastAvatarId: 0, 
            avatars: [],
            //Avatars are stacked if some "subprocess" with another avatars occurs:
            //for ex. if-avatars are processed first
            stack: []            
        };

        const avatar = {                
            runePos: startRunePos,
            prevRunePos: startRunePos,
            pos: castPos,
            dir: castDir
        };
        
        createAvatar(spell, avatar);
        
        H.game.spells.spells.push(spell);
        H.game.spells.lastSpellId++;
        
        execInstr(spell, avatar);

        return spell;
    };

    const step = function(spell) {
        if(spell.error)
            return;

        //Clone list to avoid processing 
        //newly added (forked) avatars
        const avs = _.clone(spell.avatars);
        for(const [i,a] of avs.entries()) {
            const stackAv = stepAvatar(spell, a); 
            if(stackAv) { //Stack current avatar execution (usually, on if rune)
                spell.stack.push({ avatars: avs, i: i });
                spell.avatars = [ stackAv ];
                break;
            }
        }
    };

    const destroySpell = function(spell) {
        const avs = _.clone(spell.avatars);
        for(const a of avs) 
            dieAvatar(spell, a);

        _.pull(H.game.spells.spells, spell);
    };

    return {
        rotatableRunes: [
            'arrow',
            'move',
            'if'
        ],

        launch, 
        step,
        init,
        destroySpell
    };
})();