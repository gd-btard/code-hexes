precision mediump float;

varying vec2 vTextureCoord;
varying vec4 vColor;

uniform sampler2D uSampler;
uniform float strength;

void main(void)
{
   vec2 uvs = vTextureCoord.xy;

   vec4 fg = texture2D(uSampler, vTextureCoord);

   //Premultiplied alpha is hard
   fg.r = (fg.r / fg.a + strength) * fg.a;
   fg.g = (fg.g / fg.a + strength) * fg.a;
   fg.b = (fg.b / fg.a + strength) * fg.a;

   gl_FragColor = fg;

}